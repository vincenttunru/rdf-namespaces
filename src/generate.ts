import { generateFiles } from "./generateFiles";
import { Mirrors } from "./generateNamespaceTs";

export type Namespaces = { [alias: string]: string };
const namespacesToGenerate: Namespaces = {
  acl: 'http://www.w3.org/ns/auth/acl#',
  arg: 'http://www.w3.org/ns/pim/arg#',
  as: 'http://www.w3.org/ns/activitystreams#',
  cal: 'http://www.w3.org/2002/12/cal/ical#',
  contact: 'http://www.w3.org/2000/10/swap/pim/contact#',
  dc: 'http://purl.org/dc/elements/1.1/',
  dct: 'http://purl.org/dc/terms/',
  fhir: 'http://hl7.org/fhir/',
  foaf: 'http://xmlns.com/foaf/0.1/',
  http: 'http://www.w3.org/2007/ont/http#',
  hydra: 'http://www.w3.org/ns/hydra/core#',
  ldp: 'http://www.w3.org/ns/ldp#',
  link: 'http://www.w3.org/2007/ont/link#',
  log: 'http://www.w3.org/2000/10/swap/log#',
  meeting: 'http://www.w3.org/ns/pim/meeting#',
  owl: 'http://www.w3.org/2002/07/owl#',
  qu: 'http://www.w3.org/2000/10/swap/pim/qif#',
  trip: 'http://www.w3.org/ns/pim/trip#',
  rdf: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
  rdfs: 'http://www.w3.org/2000/01/rdf-schema#',
  sched: 'http://www.w3.org/ns/pim/schedule#',
  schema: 'http://schema.org/',
  schema_https: 'https://schema.org/',
  sec: 'https://w3id.org/security#',
  sioc: 'http://rdfs.org/sioc/ns#',
  skos: 'http://www.w3.org/2004/02/skos/core#',
  solid: 'http://www.w3.org/ns/solid/terms#',
  space: 'http://www.w3.org/ns/pim/space#',
  tab: 'http://www.w3.org/2007/ont/link#',
  tabont: 'http://www.w3.org/2007/ont/link#',
  vcard: 'http://www.w3.org/2006/vcard/ns#',
  wf: 'http://www.w3.org/2005/01/wf/flow#',
};
const mirrors: Mirrors = {
  'http://schema.org/': 'https://schema.org/version/latest/schemaorg-all-http.ttl',
  'https://schema.org/': 'https://schema.org/version/latest/schemaorg-all-https.ttl',
  'http://www.w3.org/ns/activitystreams#': 'http://www.w3.org/ns/activitystreams-owl',
  'http://xmlns.com/foaf/0.1/': 'http://xmlns.com/foaf/0.1/index.rdf',
  'http://www.w3.org/ns/ldp#': 'https://www.w3.org/ns/ldp.rdf',
  'http://www.w3.org/2002/12/cal/ical#': 'https://www.w3.org/2002/12/cal/ical.rdf',
  'http://hl7.org/fhir/': 'https://www.hl7.org/fhir/fhir.ttl',
};

generateFiles(namespacesToGenerate, { mirrors });

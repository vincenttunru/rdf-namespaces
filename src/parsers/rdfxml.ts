import { getSourceUrl, Parser } from "@inrupt/solid-client";

/**
 * ```{note} This function is still experimental and subject to change, even
 * in a non-major release.
 * ```
 * This returns a parser that transforms a JSON-LD string into a set of RDFJS quads.
 *
 * @returns A Parser object.
 * @since 1.15.0
 */
export const getRdfxmlParser = (options?: { baseUrl?: string }): Parser => {
  const onQuadCallbacks: Array<Parameters<Parser["onQuad"]>[0]> = [];
  const onCompleteCallbacks: Array<Parameters<Parser["onComplete"]>[0]> = [];
  const onErrorCallbacks: Array<Parameters<Parser["onError"]>[0]> = [];

  return {
    onQuad: (callback) => {
      onQuadCallbacks.push(callback);
    },
    onError: (callback) => {
      onErrorCallbacks.push(callback);
    },
    onComplete: (callback) => {
      onCompleteCallbacks.push(callback);
    },
    parse: async (source, resourceInfo) => {
      const rdfxml = (await import('rdfxml-streaming-parser'));
      const parser = new rdfxml.RdfXmlParser({
        baseIRI: options?.baseUrl ?? getSourceUrl(resourceInfo),
      });

      parser.on('error', (error) => {
        onErrorCallbacks.forEach((callback) => callback(error));
      });
      parser.on('data', (quad) => {
        onQuadCallbacks.forEach((callback) => callback(quad));
      });
      parser.on('end', () => {
        onCompleteCallbacks.forEach((callback) => callback());
      });
      parser.write(source);
      parser.end();
    },
  };
};

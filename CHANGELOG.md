# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.12.0] - 2023-09-12

### New features

- The Security Vocabulary is now also included. For more information,
  see https://w3c.github.io/vc-data-integrity/vocab/security/vocabulary.html

## [1.11.0] - 2023-04-21

### New features

- The HL7 FHIR vocabulary is now also included. For more information,
  see https://www.hl7.org/fhir/rdf.html

## [1.10.1] - 2023-02-21

### Bugs fixed

- A behind-the-scenes change should make the fetching of vocabularies at build
  time more robust, hopefully avoiding regressions in the future. Compared to
  v1.10.0 specifically, the FOAF, Hydra and Solid vocabularies should be
  included again.

## [1.10.0] - 2023-02-19

### New features

- The [HTTPS version of Schema.org](https://schema.org/docs/releases.html#v15.0)
  is now available as `schema_https`. For backwards compatibility, `schema` is
  still referring to the HTTP version.

## [1.9.2] - 2020-09-01

### Bugs fixed

- The ActivityStreams vocabulary defines the type `Object`, which is also a global object in
  JavaScript. Global objects now also carry the `__workaround` suffix, just like reserved JavaScript
  keywords.

## [1.9.1] - 2020-09-01

### Bugs fixed

- The unavailability of a vocabulary during generation of the package led to not all required files
  being generated. rdf-namespaces now has proper error handling that simply excludes the
  unavailable package from the generated exports.
- Additionally, if a vocabulary is unavailable, we will now retry fetching it a couple of times in
  case the downtime is temporary.

## [1.9.0] - 2020-08-31

### New features

- The ActivityStreams vocabulary is now also included. For more information,
  see https://www.w3.org/ns/activitystreams.

## [1.8.0] - 2020-01-14

### New features

- Values that clash with [Javascript's reserved
  keywords](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Lexical_grammar#Keywords)
  are now exported as well, suffixed with `__workaround`.

## [1.7.0] - 2019-12-16

### New features

- The npm package now [tells Webpack that its exports are side-effect
  free](https://webpack.js.org/guides/tree-shaking/#mark-the-file-as-side-effect-free). This should
  help it to remove unused exports (usually, most of them) from your bundle.
- Updates to the RDF namespace have been incorporated. For more info, see
  https://lists.w3.org/Archives/Public/semantic-web/2019Dec/0027.html.

## [1.6.0] - 2019-11-13

### New features

- The Hydra vocabulary (for Hypermedia-Driven Web APIs) is now also included. For more information,
  see https://www.hydra-cg.com. Thanks Chris Wilkinson (@thewilkybarkid) for the contribution!

## [1.5.0] - 2019-10-25

### New features

- The Simple Knowledge Organization System (SKOS) vocabulary is now also included.

## [1.4.0] - 2019-10-01

### New features

- OWL Datatype Properties are now also included.

## [1.3.1] - 2019-09-18

### Bugs fixed

- Some properties were exported that were actually part of a different namespace than the one they were listed under.

## [1.3.0] - 2019-09-08

### New features

- A version of the code using ES Modules instead of CommonJS modules is now also published. Find it
  under the `module` key in `package.json`.

## [1.2.0] - 2019-09-04

### New features

- The full URL that will be imported is now included in each export's TSDoc.

## [1.1.2] - 2019-08-28

### Bugs fixed

- README listing an incorrect package name.

## [1.1.1] - 2019-08-21

### Bugs fixed

- tripledoc is not actually needed for consumers of this library, yet was listed as a regular dependency. It is now a devDependency.

## [1.1.0] - 2019-08-21

### New features

- Schema.org is now exported as well

## [1.0.0] - 2019-08-21

### New features

- First release!
